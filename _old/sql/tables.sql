DROP TABLE IF EXISTS `game_game_category`;
DROP TABLE IF EXISTS `functions_barshop`;
DROP TABLE IF EXISTS `barshop`;
DROP TABLE IF EXISTS `functions`;
DROP TABLE IF EXISTS `notation`;
DROP TABLE IF EXISTS `notation_theme`;
DROP TABLE IF EXISTS `game`;
DROP TABLE IF EXISTS `game_category`;
DROP TABLE IF EXISTS `user`;
DROP TABLE IF EXISTS `editor`;


CREATE TABLE IF NOT EXISTS `editor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `website` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(30),
  `lastname` varchar(30),
  `nickname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(500) NOT NULL,
  `date_subscribe` date NOT NULL,
  `avatar` text NOT NULL,
  `id_role` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  CONSTRAINT FK_users_id_role FOREIGN KEY (id_role) REFERENCES role(id)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `game_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `id_editor` int(11) NOT NULL,
  `synopsys` text NOT NULL,
  `image` text NOT NULL,
  `number_players` varchar(100) NOT NULL,
  `minutes_per_game` varchar(100) NOT NULL,
  `age_limit` varchar(100) NOT NULL,
  `average_price` float(10) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT FK_games_id_editor FOREIGN KEY (id_editor) REFERENCES editor(id)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `categorize_game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_game` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT FK_categorize_id_game FOREIGN KEY (id_game) REFERENCES game(id) ,
  CONSTRAINT FK_categorize_id_category FOREIGN KEY (id_category) REFERENCES game_category(id)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `notation_theme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `notation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_game` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_notation_theme` int(11) NOT NULL,
  `notation` int(11) NOT NULL,
  `limit_notation` int(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT FK_notation_id_notation_theme FOREIGN KEY (id_notation_theme) REFERENCES notation_theme(id),
  CONSTRAINT FK_notation_id_user FOREIGN KEY (id_user) REFERENCES user(id),
  CONSTRAINT FK_notation_id_game FOREIGN KEY (id_game) REFERENCES game(id)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `functions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`) 
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `barshop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `word_of_the_boss` text NOT NULL,
  `address` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `barshop_functions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_function` int(11) NOT NULL,
  `id_barshop` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT FK_functionize_id_functions FOREIGN KEY (id_function) REFERENCES functions(id) ,
  CONSTRAINT FK_functionize_id_barshop FOREIGN KEY (id_barshop) REFERENCES barshop(id) 
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;



INSERT INTO `editor` (`id`, `name`, `website`, `address`) VALUES
(1, 'Gigamic', 'https://www.gigamic.com/', 'Parc naturel régional des Caps et Marais d Opale, 22 Rue Jean Marie Bourguignon, 62930 Wimereux'),
(2, 'Asmodee', 'https://corporate.asmodee.com/', '18 Rue Jacqueline Auriol, 78280 Guyancourt'),
(3, 'Bombyx', 'https://studiobombyx.com/', '8 Rue du Palais, 29000 Quimper'),
(4, 'Cocktailgames', 'http://www.cocktailgames.com/', '2 Rue du Hazard, 78000 Versailles'),
(5, 'Libellud', 'http://www.libellud.com/', '23 Rue Alsace Lorraine, 86000 Poitiers'),
(6, 'Edge Entertainment', 'http://www.edgeent.fr/', '1 Rond-Point de Flotis, Bâtiment 3, 1er Étage, 31240 Saint-Jean'),
(7, 'Iello', 'https://www.iello.com/', '9 Avenue des Érables, 54180 Heillecourt');


INSERT INTO `functions` (`id`, `name`) VALUES
(1, 'Alcool'),
(2, 'Tapas'),
(3, 'Essayer des jeux gratuitement'),
(4, 'Ici on peut jouer (cotisation)'),
(5, 'Ici on peut jouer (gratuit)'),
(6, 'Restauration'),
(7, 'Boissons');


INSERT INTO `user` (`id`, `id_role`, `firstname`, `lastname`, `nickname`, `email`, `password`, `date_subscribe`, `avatar`) VALUES
(1, 1, 'Admin', 'admin', 'Administrateur', 'admin@ludopawn.com', 'provisoire (a remplacer par la hash généré et choisi)', '2019-10-07', 'path to file on server'),
(2, 2, 'Jeffrey', 'JEFFERSON', 'Jeff', 'jeff.jefferson@gmail.com', 'provisoire (a remplacer par la hash généré et choisi)', '2019-10-08', 'path to file on server'),
(3, 3, 'Andrew', 'STEINMAN', 'The_Expert', 'andy.stein@hotmail.com', '(provisoire)', '2019-10-09', 'path to file on server'),
(4, 3, 'Marie-Stéphanie', 'DE LA CONCORDIÉRE', 'Marie-Stef', 'marie.s.cathofasciste@gmail.com', '(provisoire)', '2019-10-10', 'path file on server');

COMMIT;
