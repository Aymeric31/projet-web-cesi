<?php

namespace App\Form;

use App\Entity\NotationGroup;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NotationGroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rating_1', HiddenType::class, [
                'label' => false,
                'required' => true,
                'data' => 0
            ])
            ->add('rating_2', HiddenType::class, [
                'label' => false,
                'required' => true,
                'data' => 0
            ])
            ->add('rating_3', HiddenType::class, [
                'label' => false,
                'required' => true,
                'data' => 0
            ])
            ->add('rating_4', HiddenType::class, [
                'label' => false,
                'required' => true,
                'data' => 0
            ])
            ->add('rating_5', HiddenType::class, [
                'label' => false,
                'required' => true,
                'data' => 0
            ])
            ->add('comment', TextareaType::class, [
                'label' => false,
                'required' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => NotationGroup::class,
        ]);
    }
}
