<?php

namespace App\Form;

use App\Entity\BarshopSearch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BarshopSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('city', TextType::class, [
                'required' => false,
                'label' => "Ville",
                'attr' => [
                    'placeholder' => 'Recherche par ville'
                ]
            ])
            ->add('name', TextType::class, [
                'required' => false,
                'label' => "Nom",
                'attr' => [
                    'placeholder' => 'Recherche par nom'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BarshopSearch::class,
            'method' => 'get',
            'csrf_protection' => false
        ]);
    }

    /*
     * ASTUCE
     * Cette fonction sert réduire les noms des variables de recherche dans l'URL
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
