<?php

namespace App\Form;

use App\Entity\Barshop;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BarshopType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('wordOfTheBoss')
            ->add('phone')
            ->add('email')
            ->add('website')
            ->add('type')
            ->add('address_city')
            ->add('address_zipcode')
            ->add('address_street_or_locality')
            ->add('address_num')
            ->add('address_complement')
            ->add('functions')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Barshop::class,
        ]);
    }
}
