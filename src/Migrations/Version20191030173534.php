<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191030173534 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE functions_barshop (functions_id INT NOT NULL, barshop_id INT NOT NULL, INDEX IDX_B0D1213E9011893B (functions_id), INDEX IDX_B0D1213E438FA492 (barshop_id), PRIMARY KEY(functions_id, barshop_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE functions_barshop ADD CONSTRAINT FK_B0D1213E9011893B FOREIGN KEY (functions_id) REFERENCES functions (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE functions_barshop ADD CONSTRAINT FK_B0D1213E438FA492 FOREIGN KEY (barshop_id) REFERENCES barshop (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE functions_barshop');
    }
}
