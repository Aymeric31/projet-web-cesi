<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191030184916 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE game_game_category (game_id INT NOT NULL, game_category_id INT NOT NULL, INDEX IDX_7EC7A8CE48FD905 (game_id), INDEX IDX_7EC7A8CCC13DFE0 (game_category_id), PRIMARY KEY(game_id, game_category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE game_game_category ADD CONSTRAINT FK_7EC7A8CE48FD905 FOREIGN KEY (game_id) REFERENCES game (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE game_game_category ADD CONSTRAINT FK_7EC7A8CCC13DFE0 FOREIGN KEY (game_category_id) REFERENCES game_category (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE barshop_functions');
        $this->addSql('DROP TABLE categorize_game');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE barshop_functions (id INT AUTO_INCREMENT NOT NULL, id_barshop INT DEFAULT NULL, id_function INT DEFAULT NULL, INDEX FK_functionize_id_functions (id_function), INDEX FK_functionize_id_barshop (id_barshop), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE categorize_game (id INT AUTO_INCREMENT NOT NULL, id_category INT DEFAULT NULL, id_game INT DEFAULT NULL, INDEX FK_categorize_id_game (id_game), INDEX FK_categorize_id_category (id_category), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE barshop_functions ADD CONSTRAINT FK_DC659B1F924CD72C FOREIGN KEY (id_barshop) REFERENCES barshop (id)');
        $this->addSql('ALTER TABLE barshop_functions ADD CONSTRAINT FK_DC659B1F9A75FAA8 FOREIGN KEY (id_function) REFERENCES functions (id)');
        $this->addSql('ALTER TABLE categorize_game ADD CONSTRAINT FK_BF0164AB5697F554 FOREIGN KEY (id_category) REFERENCES game_category (id)');
        $this->addSql('ALTER TABLE categorize_game ADD CONSTRAINT FK_BF0164ABA80B2D8E FOREIGN KEY (id_game) REFERENCES game (id)');
        $this->addSql('DROP TABLE game_game_category');
    }
}
