<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191024120122 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE barshop (id INT AUTO_INCREMENT NOT NULL, name TINYTEXT NOT NULL, description TEXT NOT NULL, word_of_the_boss TEXT NOT NULL, address VARCHAR(100) DEFAULT NULL, phone VARCHAR(20) DEFAULT NULL, email VARCHAR(100) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE barshop_functions (id INT AUTO_INCREMENT NOT NULL, id_barshop INT DEFAULT NULL, id_function INT DEFAULT NULL, INDEX FK_functionize_id_barshop (id_barshop), INDEX FK_functionize_id_functions (id_function), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE categorize_game (id INT AUTO_INCREMENT NOT NULL, id_category INT DEFAULT NULL, id_game INT DEFAULT NULL, INDEX FK_categorize_id_category (id_category), INDEX FK_categorize_id_game (id_game), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE editor (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, website VARCHAR(100) DEFAULT NULL, address VARCHAR(100) DEFAULT NULL, UNIQUE INDEX name (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE functions (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE game (id INT AUTO_INCREMENT NOT NULL, id_editor INT DEFAULT NULL, name VARCHAR(100) NOT NULL, synopsys TEXT NOT NULL, image TEXT NOT NULL, number_players VARCHAR(100) NOT NULL, minutes_per_game VARCHAR(100) NOT NULL, age_limit VARCHAR(100) NOT NULL, average_price DOUBLE PRECISION NOT NULL, INDEX FK_game_id_editor (id_editor), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE game_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notation (id INT AUTO_INCREMENT NOT NULL, id_game INT DEFAULT NULL, id_notation_theme INT DEFAULT NULL, id_user INT DEFAULT NULL, notation INT NOT NULL, limit_notation INT NOT NULL, ponderation INT NOT NULL, date DATE NOT NULL, INDEX FK_notation_id_user (id_user), INDEX FK_notation_id_game (id_game), INDEX FK_notation_id_notation_theme (id_notation_theme), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notation_theme (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, weakword VARCHAR(100) NOT NULL, strongword VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, firstname VARCHAR(50) NOT NULL, lastname VARCHAR(50) NOT NULL, date_subscribe DATETIME NOT NULL, avatar VARCHAR(255) DEFAULT NULL, username VARCHAR(50) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE barshop_functions ADD CONSTRAINT FK_DC659B1F924CD72C FOREIGN KEY (id_barshop) REFERENCES barshop (id)');
        $this->addSql('ALTER TABLE barshop_functions ADD CONSTRAINT FK_DC659B1F9A75FAA8 FOREIGN KEY (id_function) REFERENCES functions (id)');
        $this->addSql('ALTER TABLE categorize_game ADD CONSTRAINT FK_BF0164AB5697F554 FOREIGN KEY (id_category) REFERENCES game_category (id)');
        $this->addSql('ALTER TABLE categorize_game ADD CONSTRAINT FK_BF0164ABA80B2D8E FOREIGN KEY (id_game) REFERENCES game (id)');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318CEAC64457 FOREIGN KEY (id_editor) REFERENCES editor (id)');
        $this->addSql('ALTER TABLE notation ADD CONSTRAINT FK_268BC95A80B2D8E FOREIGN KEY (id_game) REFERENCES game (id)');
        $this->addSql('ALTER TABLE notation ADD CONSTRAINT FK_268BC9545A1DFF5 FOREIGN KEY (id_notation_theme) REFERENCES notation_theme (id)');
        $this->addSql('ALTER TABLE notation ADD CONSTRAINT FK_268BC956B3CA4B FOREIGN KEY (id_user) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE barshop_functions DROP FOREIGN KEY FK_DC659B1F924CD72C');
        $this->addSql('ALTER TABLE game DROP FOREIGN KEY FK_232B318CEAC64457');
        $this->addSql('ALTER TABLE barshop_functions DROP FOREIGN KEY FK_DC659B1F9A75FAA8');
        $this->addSql('ALTER TABLE categorize_game DROP FOREIGN KEY FK_BF0164ABA80B2D8E');
        $this->addSql('ALTER TABLE notation DROP FOREIGN KEY FK_268BC95A80B2D8E');
        $this->addSql('ALTER TABLE categorize_game DROP FOREIGN KEY FK_BF0164AB5697F554');
        $this->addSql('ALTER TABLE notation DROP FOREIGN KEY FK_268BC9545A1DFF5');
        $this->addSql('ALTER TABLE notation DROP FOREIGN KEY FK_268BC956B3CA4B');
        $this->addSql('DROP TABLE barshop');
        $this->addSql('DROP TABLE barshop_functions');
        $this->addSql('DROP TABLE categorize_game');
        $this->addSql('DROP TABLE editor');
        $this->addSql('DROP TABLE functions');
        $this->addSql('DROP TABLE game');
        $this->addSql('DROP TABLE game_category');
        $this->addSql('DROP TABLE notation');
        $this->addSql('DROP TABLE notation_theme');
        $this->addSql('DROP TABLE user');
    }
}
