<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Functions
 *
 * @ORM\Table(name="functions")
 * @ORM\Entity(repositoryClass="App\Repository\FunctionsRepository")
 */
class Functions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\barshop", inversedBy="functions")
     */
    private $barshops;

    public function __construct()
    {
        $this->barshops = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|barshop[]
     */
    public function getBarshops(): Collection
    {
        return $this->barshops;
    }

    public function addBarshop(barshop $barshop): self
    {
        if (!$this->barshops->contains($barshop)) {
            $this->barshops[] = $barshop;
        }

        return $this;
    }

    public function removeBarshop(barshop $barshop): self
    {
        if ($this->barshops->contains($barshop)) {
            $this->barshops->removeElement($barshop);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}
