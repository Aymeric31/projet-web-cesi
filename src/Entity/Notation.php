<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notation
 *
 * @ORM\Table(name="notation", indexes={@ORM\Index(name="FK_notation_id_user", columns={"id_user"}), @ORM\Index(name="FK_notation_id_game", columns={"id_game"}), @ORM\Index(name="FK_notation_id_notation_theme", columns={"id_notation_theme"})})
 * @ORM\Entity(repositoryClass="App\Repository\NotationRepository")
 */
class Notation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="notation", type="integer", nullable=false)
     */
    private $notation;

    /**
     * @var int
     *
     * @ORM\Column(name="ponderation", type="integer", nullable=false)
     */
    private $ponderation;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=100, nullable=true)
     */
    private $comment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var \Game
     *
     * @ORM\ManyToOne(targetEntity="Game")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_game", referencedColumnName="id")
     * })
     */
    private $game;

    /**
     * @var \NotationTheme
     *
     * @ORM\ManyToOne(targetEntity="NotationTheme")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_notation_theme", referencedColumnName="id")
     * })
     */
    private $notationTheme;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNotation(): ?int
    {
        return $this->notation;
    }

    public function setNotation(int $notation): self
    {
        $this->notation = $notation;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getPonderation(): ?int
    {
        return $this->ponderation;
    }

    public function setPonderation(int $ponderation): self
    {
        $this->ponderation = $ponderation;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getGame(): ?Game
    {
        return $this->game;
    }

    public function setGame(?Game $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function getNotationTheme(): ?NotationTheme
    {
        return $this->notationTheme;
    }

    public function setNotationTheme(?NotationTheme $notationTheme): self
    {
        $this->notationTheme = $notationTheme;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function __toString()
    {
        return "Notation ID: ".((string) $this->id);
    }
}
