<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Editors;

/**
 * Game
 *
 * @ORM\Table(name="game", indexes={@ORM\Index(name="FK_game_id_editor", columns={"id_editor"})})
 * @ORM\Entity(repositoryClass="App\Repository\GameRepository")
 */
class Game
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="synopsys", type="text", length=65535, nullable=false)
     */
    private $synopsys;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="text", length=65535, nullable=false)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="number_players", type="string", length=100, nullable=false)
     */
    private $numberPlayers;

    /**
     * @var string
     *
     * @ORM\Column(name="minutes_per_game", type="string", length=100, nullable=false)
     */
    private $minutesPerGame;

    /**
     * @var string
     *
     * @ORM\Column(name="age_limit", type="string", length=100, nullable=false)
     */
    private $ageLimit;

    /**
     * @var float
     *
     * @ORM\Column(name="average_price", type="float", precision=10, scale=0, nullable=false)
     */
    private $averagePrice;

    /**
     * @var Editor
     *
     * @ORM\ManyToOne(targetEntity="Editor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_editor", referencedColumnName="id")
     * })
     */
    private $editor;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\GameCategory", inversedBy="games")
     * @ORM\JoinTable(name="game_game_category")
     */
    private $categories;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSynopsys(): ?string
    {
        return $this->synopsys;
    }

    public function setSynopsys(string $synopsys): self
    {
        $this->synopsys = $synopsys;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getNumberPlayers(): ?string
    {
        return $this->numberPlayers;
    }

    public function setNumberPlayers(string $numberPlayers): self
    {
        $this->numberPlayers = $numberPlayers;

        return $this;
    }

    public function getMinutesPerGame(): ?string
    {
        return $this->minutesPerGame;
    }

    public function setMinutesPerGame(string $minutesPerGame): self
    {
        $this->minutesPerGame = $minutesPerGame;

        return $this;
    }

    public function getAgeLimit(): ?string
    {
        return $this->ageLimit;
    }

    public function setAgeLimit(string $ageLimit): self
    {
        $this->ageLimit = $ageLimit;

        return $this;
    }

    public function getAveragePrice(): ?float
    {
        return $this->averagePrice;
    }

    public function setAveragePrice(float $averagePrice): self
    {
        $this->averagePrice = $averagePrice;

        return $this;
    }

    public function getEditor(): ?Editor
    {
        return $this->editor;
    }

    public function setEditor(?Editor $editor): self
    {
        $this->editor = $editor;

        return $this;
    }

    /**
     * @return Collection|GameCategory[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(GameCategory $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    public function removeCategory(GameCategory $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}
