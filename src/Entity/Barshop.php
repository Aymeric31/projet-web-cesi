<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Barshop
 *
 * @ORM\Table(name="barshop")
 * @ORM\Entity(repositoryClass="App\Repository\BarshopRepository")
 */
class Barshop
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="word_of_the_boss", type="text", length=65535, nullable=false)
     */
    private $wordOfTheBoss;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="text", length=65535, nullable=false)
     */
    private $image;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone", type="string", length=20, nullable=true)
     */
    private $phone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="website", type="string", length=100, nullable=true)
     */
    private $website;

    /**
     * @var string
     *
     * @ORM\Column(name="type", name="type", type="string", length=20, nullable=false)
     */
    private $type;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Functions", mappedBy="barshops")
     */
    private $functions;

    /**
     * @ORM\Column(name="addr_city", type="string", length=50, nullable=true)
     */
    private $address_city;

    /**
     * @ORM\Column(name="addr_zip", type="string", length=10, nullable=true)
     */
    private $address_zipcode;

    /**
     * @ORM\Column(name="addr_street", type="string", length=255, nullable=true)
     */
    private $address_street_or_locality;

    /**
     * @ORM\Column(name="addr_num", type="string", length=20, nullable=true)
     */
    private $address_num;

    /**
     * @ORM\Column(name="addr_compl", type="string", length=255, nullable=true)
     */
    private $address_complement;

    public function __construct()
    {
        $this->functions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getWordOfTheBoss(): ?string
    {
        return $this->wordOfTheBoss;
    }

    public function setWordOfTheBoss(string $wordOfTheBoss): self
    {
        $this->wordOfTheBoss = $wordOfTheBoss;

        return $this;
    }

    public function getAddressCity(): ?string
    {
        return $this->address_city;
    }

    public function setAddressCity(?string $address_city): self
    {
        $this->address_city = $address_city;

        return $this;
    }

    public function getAddressZipcode(): ?string
    {
        return $this->address_zipcode;
    }

    public function setAddressZipcode(?string $address_zipcode): self
    {
        $this->address_zipcode = $address_zipcode;

        return $this;
    }

    public function getAddressStreetOrLocality(): ?string
    {
        return $this->address_street_or_locality;
    }

    public function setAddressStreetOrLocality(?string $address_street_or_locality): self
    {
        $this->address_street_or_locality = $address_street_or_locality;

        return $this;
    }

    public function getAddressNum(): ?string
    {
        return $this->address_num;
    }

    public function setAddressNum(?string $address_num): self
    {
        $this->address_num = $address_num;

        return $this;
    }

    public function getAddressComplement(): ?string
    {
        return $this->address_complement;
    }

    public function setAddressComplement(?string $address_complement): self
    {
        $this->address_complement = $address_complement;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $possible_types = array("Bar", "bar", "Boutique", "boutique");
        if(in_array($type, $possible_types)){
            $this->type = strtolower($type);
        }
        else{
            $this->type = "Aucun";
        }
        return $this;
    }

    /**
     * @return Collection|Functions[]
     */
    public function getFunctions(): Collection
    {
        return $this->functions;
    }

    public function addFunction(Functions $function): self
    {
        if (!$this->functions->contains($function)) {
            $this->functions[] = $function;
            $function->addBarshop($this);
        }

        return $this;
    }

    public function removeFunction(Functions $function): self
    {
        if ($this->functions->contains($function)) {
            $this->functions->removeElement($function);
            $function->removeBarshop($this);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }

}
