<?php

namespace App\Entity;

class BarshopSearch{

    /*
     * @var string|null
     */
    private $city;

    /*
     * @var string|null
     */
    private $name;

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return BarshopSearch
     */
    public function setCity($city): BarshopSearch
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return BarshopSearch
     */
    public function setName($name): BarshopSearch
    {
        $this->name = $name;
        return $this;
    }
}
