<?php
namespace App\Entity;

class NotationGroup{

    /*
     * @var int
     */
    private $rating_1;

    /*
     * @var int
     */
    private $rating_2;

    /*
     * @var int
     */
    private $rating_3;

    /*
     * @var int
     */
    private $rating_4;

    /*
     * @var int
     */
    private $rating_5;

    /*
     * @var string
     */
    private $comment;

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     * @return NotationGroup
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return int
     */
    public function getRating1()
    {
        return $this->rating_1;
    }

    /**
     * @param int $rating_1
     * @return NotationGroup
     */
    public function setRating1(int $rating_1)
    {
        $this->rating_1 = $rating_1;
        return $this;
    }

    /**
     * @return int
     */
    public function getRating2()
    {
        return $this->rating_2;
    }

    /**
     * @param int $rating_2
     * @return NotationGroup
     */
    public function setRating2(int $rating_2)
    {
        $this->rating_2 = $rating_2;
        return $this;
    }

    /**
     * @return int
     */
    public function getRating3()
    {
        return $this->rating_3;
    }

    /**
     * @param int $rating_3
     * @return NotationGroup
     */
    public function setRating3(int $rating_3)
    {
        $this->rating_3 = $rating_3;
        return $this;
    }

    /**
     * @return int
     */
    public function getRating4()
    {
        return $this->rating_4;
    }

    /**
     * @param int $rating_4
     * @return NotationGroup
     */
    public function setRating4(int $rating_4)
    {
        $this->rating_4 = $rating_4;
        return $this;
    }

    /**
     * @return int
     */
    public function getRating5()
    {
        return $this->rating_5;
    }

    /**
     * @param int $rating_5
     * @return NotationGroup
     */
    public function setRating5(int $rating_5)
    {
        $this->rating_5 = $rating_5;
        return $this;
    }
}