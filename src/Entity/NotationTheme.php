<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NotationTheme
 *
 * @ORM\Table(name="notation_theme")
 * @ORM\Entity(repositoryClass="App\Repository\NotationThemeRepository")
 */
class NotationTheme
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="weakword", type="string", length=100, nullable=false)
     */
    private $weakWord;

    /**
     * @var string
     *
     * @ORM\Column(name="strongword", type="string", length=100, nullable=false)
     */
    private $strongWord;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $icon;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $color;

    /**
     * @var int
     *
     * @ORM\Column(name="limit_notation", type="integer", nullable=false)
     */
    private $limitNotation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getWeakWord(): ?string
    {
        return $this->weakWord;
    }

    public function setWeakWord(string $weakWord): self
    {
        $this->weakWord = $weakWord;

        return $this;
    }

    public function getStrongWord(): ?string
    {
        return $this->strongWord;
    }

    public function setStrongWord(string $strongWord): self
    {
        $this->strongWord = $strongWord;

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    public function getLimitNotation(): ?int
    {
        return $this->limitNotation;
    }

    public function setLimitNotation(int $limitNotation): self
    {
        $this->limitNotation = $limitNotation;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor($color): self
    {
        $this->color = $color;
        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}
