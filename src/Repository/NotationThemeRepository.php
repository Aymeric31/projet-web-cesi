<?php

namespace App\Repository;

use App\Entity\NotationTheme;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method NotationTheme|null find($id, $lockMode = null, $lockVersion = null)
 * @method NotationTheme|null findOneBy(array $criteria, array $orderBy = null)
 * @method NotationTheme[]    findAll()
 * @method NotationTheme[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NotationThemeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NotationTheme::class);
    }

    // /**
    //  * @return NotationTheme[] Returns an array of NotationTheme objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NotationTheme
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
