<?php

namespace App\Repository;

use App\Entity\GameCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

/**
 * @method GameCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method GameCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method GameCategory[]    findAll()
 * @method GameCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GameCategory::class);
    }

    /**
     * @param $category
     * @return QueryBuilder
     */
    public function findAllByCategory($category)
    {
        /*
        $toto = $this->createQueryBuilder('c')
            ->addSelect('g')
            ->andWhere('c.id = :cat')
            ->leftJoin('game', 'g')
            ->leftJoin('game_game_category', 'ggc')
            ->setParameter('cat', $category)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(100);
        */
    }


}
