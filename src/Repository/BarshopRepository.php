<?php

namespace App\Repository;

use App\Entity\Barshop;
use App\Entity\BarshopSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Barshop|null find($id, $lockMode = null, $lockVersion = null)
 * @method Barshop|null findOneBy(array $criteria, array $orderBy = null)
 * @method Barshop[]    findAll()
 * @method Barshop[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BarshopRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Barshop::class);
    }

    /**
     * @return Barshop[] Returns an array of Barshop objects
     */
    public function findAllByType($type, $orderBy, BarshopSearch $search)
    {
        $query = $this->createQueryBuilder('b')
            ->andWhere('b.type = :val')
            ->setParameter('val', $type)
            ->orderBy('b.id', $orderBy)
            ->setMaxResults(100)
        ;

        if ($search->getCity()){
            $query = $query->andWhere('b.address_city LIKE :city')
                ->setParameter('city', '%'.$search->getCity().'%');
        }
        if ($search->getName()){
            $query = $query->andWhere('b.name LIKE :name')
                ->setParameter('name', '%'.$search->getName().'%');
        }

        return $query->getQuery()->getResult();
    }

    // /**
    //  * @return Barshop[] Returns an array of Barshop objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Barshop
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
