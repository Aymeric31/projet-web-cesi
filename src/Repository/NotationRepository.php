<?php

namespace App\Repository;

use App\Entity\Notation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Notation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Notation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Notation[]    findAll()
 * @method Notation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NotationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Notation::class);
    }

    /**
     * @return Notation[] Returns an array of Notation objects
     */
    public function findAllByGameAndByUser($game, $user, $orderBy)
    {
        $query = $this->createQueryBuilder('n')
            ->andWhere('n.game = :game')
            ->setParameter('game', $game)
            ->andWhere('n.user = :user')
            ->setParameter('user', $user)
            ->orderBy('n.id', $orderBy)
            ->setMaxResults(100)
        ;

        return $query->getQuery()->getResult();
    }

    public function findAllByGameAndByTheme($game, $theme){
        $query = $this->createQueryBuilder('n')
            ->andWhere('n.game = :game')
            ->setParameter('game', $game)
            ->andWhere('n.notationTheme = :theme')
            ->setParameter('theme', $theme)
            ->setMaxResults(100)
        ;

        return $query->getQuery()->getResult();
    }

    // /**
    //  * @return Notation[] Returns an array of Notation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Notation
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
