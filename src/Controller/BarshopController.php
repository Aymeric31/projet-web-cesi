<?php

namespace App\Controller;

use App\Entity\Barshop;
use App\Entity\BarshopSearch;
use App\Form\BarshopType;
use App\Form\BarshopSearchType;
use App\Repository\BarshopRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/barshop")
 */
class BarshopController extends AbstractController
{

    /**
     * @var BarshopRepository
     */
    private $barshop_repo;

    public function __construct(BarshopRepository $barshop_repo)
    {
        $this->barshop_repo = $barshop_repo;
    }

    /**
     * @Route("/bars", name="bars")
     */
    public function index(Request $request): Response
    {
        $search = new BarshopSearch();
        $form = $this->createForm(BarshopSearchType::class, $search);
        $form->handleRequest($request);

        $bars = $this->barshop_repo->findAllByType('bar', 'ASC', $search);

        return $this->render('barshop/bars.html.twig', [
            'bars' => $bars,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/shops", name="shops")
     */
    public function shops(Request $request): Response
    {
        $search = new BarshopSearch();
        $form = $this->createForm(BarshopSearchType::class, $search);
        $form->handleRequest($request);

        $shops = $this->barshop_repo->findAllByType('boutique', 'ASC', $search);

        return $this->render('barshop/shops.html.twig', [
            'shops' => $shops,
            'form' => $form->createView()
        ]);
    }

}
