<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;

use App\Entity\Barshop;
use App\Form\BarshopType;
use App\Repository\BarshopRepository;

use App\Entity\Editor;
use App\Form\EditorType;
use App\Repository\EditorRepository;

use App\Entity\Game;
use App\Form\GameType;
use App\Repository\GameRepository;

use App\Entity\GameCategory;
use App\Form\GameCategoryType;
use App\Repository\GameCategoryRepository;

use App\Entity\Functions;
use App\Form\FunctionsType;
use App\Repository\FunctionsRepository;

use App\Entity\Notation;
use App\Form\NotationType;
use App\Repository\NotationRepository;

use App\Entity\NotationTheme;
use App\Form\NotationThemeType;
use App\Repository\NotationThemeRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/", name="admin")
     */
    public function index()
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    //
    // USERS
    //
    /**
     * @Route("/users", name="user_index", methods={"GET"})
     */
    public function users(UserRepository $userRepository): Response
    {
        return $this->render('admin/user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/user/{id}", name="user_show", methods={"GET"}, requirements={"id":"\d+"})
     */
    public function show(User $user): Response
    {
        return $this->render('admin/user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/user/add", name="user_new", methods={"GET","POST"})
     */
    public function user_new(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('admin/user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/user/edit/{id}", name="user_edit", methods={"GET","POST"}, requirements={"id":"\d+"})
     */
    public function user_edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('admin/user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/user/delete/{id}", name="user_delete", methods={"DELETE"}, requirements={"id":"\d+"})
     */
    public function user_delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index');
    }


    //
    // BARSHOPS
    //
    /**
     * @Route("/barshops", name="barshop_index", methods={"GET"})
     */
    public function barshops(BarshopRepository $barshopRepository): Response
    {
        return $this->render('admin/barshop/index.html.twig', [
            'barshops' => $barshopRepositoryNotationThemeRepository,
        ]);
    }

    /**
     * @Route("/barshop/new", name="barshop_new", methods={"GET","POST"})
     */
    public function barshop_new(Request $request): Response
    {
        $barshop = new Barshop();
        $form = $this->createForm(BarshopType::class, $barshop);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($barshop);
            $entityManager->flush();

            return $this->redirectToRoute('barshop_index');
        }

        return $this->render('admin/barshop/new.html.twig', [
            'barshop' => $barshop,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/barshop/{id}", name="barshop_show", methods={"GET"}, requirements={"id":"\d+"})
     */
    public function barshop_show(Barshop $barshop): Response
    {
        return $this->render('admin/barshop/show.html.twig', [
            'barshop' => $barshop,
        ]);
    }

    /**
     * @Route("/barshop/edit/{id}", name="barshop_edit", methods={"GET","POST"}, requirements={"id":"\d+"})
     */
    public function barshop_edit(Request $request, Barshop $barshop): Response
    {
        $form = $this->createForm(BarshopType::class, $barshop);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('barshop_index');
        }

        return $this->render('admin/barshop/edit.html.twig', [
            'barshop' => $barshop,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/barshop/delete/{id}", name="barshop_delete", methods={"DELETE"}, requirements={"id":"\d+"})
     */
    public function barshop_delete(Request $request, Barshop $barshop): Response
    {
        if ($this->isCsrfTokenValid('delete'.$barshop->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($barshop);
            $entityManager->flush();
        }

        return $this->redirectToRoute('barshop_index');
    }


    //
    // EDITORS
    //
    /**
     * @Route("/editors", name="editor_index", methods={"GET"})
     */
    public function editors(EditorRepository $editorRepository): Response
    {
        return $this->render('admin/editor/index.html.twig', [
            'editors' => $editorRepository->findAll(),
        ]);
    }

    /**
     * @Route("/editor/{id}", name="editor_show", methods={"GET"}, requirements={"id":"\d+"})
     */
    public function editor_show(Editor $editor): Response
    {
        return $this->render('admin/editor/show.html.twig', [
            'editor' => $editor,
        ]);
    }

    /**
     * @Route("editor/new", name="editor_new", methods={"GET","POST"})
     */
    public function editor_new(Request $request): Response
    {
        $editor = new Editor();
        $form = $this->createForm(EditorType::class, $editor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($editor);
            $entityManager->flush();

            return $this->redirectToRoute('editor_index');
        }

        return $this->render('admin/editor/new.html.twig', [
            'editor' => $editor,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("editor/edit/{id}", name="editor_edit", methods={"GET","POST"}, requirements={"id":"\d+"})
     */
    public function editor_edit(Request $request, Editor $editor): Response
    {
        $form = $this->createForm(EditorType::class, $editor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('editor_index');
        }

        return $this->render('admin/editor/edit.html.twig', [
            'editor' => $editor,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("editor/delete/{id}", name="editor_delete", methods={"DELETE"}, requirements={"id":"\d+"})
     */
    public function editor_delete(Request $request, Editor $editor): Response
    {
        if ($this->isCsrfTokenValid('delete'.$editor->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($editor);
            $entityManager->flush();
        }

        return $this->redirectToRoute('editor_index');
    }


    //
    // GAMES
    //
    /**
     * @Route("/games", name="game_index", methods={"GET"})
     */
    public function games(GameRepository $gameRepository): Response
    {
        return $this->render('admin/game/index.html.twig', [
            'games' => $gameRepository->findAll(),
        ]);
    }

    /**
     * @Route("/game/{id}", name="game_show", methods={"GET"}, requirements={"id":"\d+"})
     */
    public function game_show(Game $game): Response
    {
        return $this->render('admin/game/show.html.twig', [
            'game' => $game,
        ]);
    }

    /**
     * @Route("/game/new", name="game_new", methods={"GET","POST"})
     */
    public function game_new(Request $request): Response
    {
        $game = new Game();
        $form = $this->createForm(GameType::class, $game);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($game);
            $entityManager->flush();

            return $this->redirectToRoute('game_index');
        }

        return $this->render('admin/game/new.html.twig', [
            'game' => $game,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("game/edit/{id}", name="game_edit", methods={"GET","POST"}, requirements={"id":"\d+"})
     */
    public function edit(Request $request, Game $game): Response
    {
        $form = $this->createForm(GameType::class, $game);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('game_index');
        }

        return $this->render('admin/game/edit.html.twig', [
            'game' => $game,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/game/delete/{id}", name="game_delete", methods={"DELETE"}, requirements={"id":"\d+"})
     */
    public function game_delete(Request $request, Game $game): Response
    {
        if ($this->isCsrfTokenValid('delete'.$game->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($game);
            $entityManager->flush();
        }

        return $this->redirectToRoute('game_index');
    }


    //
    // CATEGORIES (game_category)
    //
    /**
     * @Route("/categories", name="category_index", methods={"GET"})
     */
    public function categories(GameCategoryRepository $gameCategoryRepository): Response
    {
        return $this->render('admin/category/index.html.twig', [
            'game_categories' => $gameCategoryRepository->findAll(),
        ]);
    }

    /**
     * @Route("/category/{id}", name="category_show", methods={"GET"}, requirements={"id":"\d+"})
     */
    public function category_show(GameCategory $gameCategory): Response
    {
        return $this->render('admin/category/show.html.twig', [
            'game_category' => $gameCategory,
        ]);
    }

    /**
     * @Route("/category/new", name="category_new", methods={"GET","POST"})
     */
    public function category_new(Request $request): Response
    {
        $gameCategory = new GameCategory();
        $form = $this->createForm(GameCategoryType::class, $gameCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($gameCategory);
            $entityManager->flush();

            return $this->redirectToRoute('game_category_index');
        }

        return $this->render('admin/category/new.html.twig', [
            'game_category' => $gameCategory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/category/edit/{id}", name="category_edit", methods={"GET","POST"}, requirements={"id":"\d+"})
     */
    public function category_edit(Request $request, GameCategory $gameCategory): Response
    {
        $form = $this->createForm(GameCategoryType::class, $gameCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('game_category_index');
        }

        return $this->render('admin/category/edit.html.twig', [
            'game_category' => $gameCategory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/category/delete/{id}", name="category_delete", methods={"DELETE"}, requirements={"id":"\d+"})
     */
    public function category_delete(Request $request, GameCategory $gameCategory): Response
    {
        if ($this->isCsrfTokenValid('delete'.$gameCategory->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($gameCategory);
            $entityManager->flush();
        }

        return $this->redirectToRoute('category_index');
    }


    //
    // FUNCTIONS
    //
    /**
     * @Route("/functions", name="functions_index", methods={"GET"})
     */
    public function functions(FunctionsRepository $functionsRepository): Response
    {
        return $this->render('admin/functions/index.html.twig', [
            'functions' => $functionsRepository->findAll(),
        ]);
    }

    /**
     * @Route("/function/{id}", name="functions_show", methods={"GET"}, requirements={"id":"\d+"})
     */
    public function function_show(Functions $function): Response
    {
        return $this->render('admin/functions/show.html.twig', [
            'function' => $function,
        ]);
    }

    /**
     * @Route("/function/new", name="functions_new", methods={"GET","POST"})
     */
    public function function_new(Request $request): Response
    {
        $function = new Functions();
        $form = $this->createForm(FunctionsType::class, $function);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($function);
            $entityManager->flush();

            return $this->redirectToRoute('functions_index');
        }

        return $this->render('admin/functions/new.html.twig', [
            'function' => $function,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/function/edit/{id}", name="functions_edit", methods={"GET","POST"}, requirements={"id":"\d+"})
     */
    public function function_edit(Request $request, Functions $function): Response
    {
        $form = $this->createForm(FunctionsType::class, $function);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('functions_index');
        }

        return $this->render('admin/functions/edit.html.twig', [
            'function' => $function,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/function/delete/{id}", name="functions_delete", methods={"DELETE"}, requirements={"id":"\d+"})
     */
    public function function_delete(Request $request, Functions $function): Response
    {
        if ($this->isCsrfTokenValid('delete'.$function->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($function);
            $entityManager->flush();
        }

        return $this->redirectToRoute('functions_index');
    }

    //
    // NOTATION
    //
    /**
     * @Route("/notations", name="notation_index", methods={"GET"})
     */
    public function notations(NotationRepository $notationRepository): Response
    {
        return $this->render('admin/notation/index.html.twig', [
            'notations' => $notationRepository->findAll(),
        ]);
    }

    /**
     * @Route("/notation/{id}", name="notation_show", methods={"GET"}, requirements={"id":"\d+"})
     */
    public function notation_show(Notation $notation): Response
    {
        return $this->render('admin/notation/show.html.twig', [
            'notation' => $notation,
        ]);
    }

    /**
     * @Route("/notation/new", name="notation_new", methods={"GET","POST"})
     */
    public function notation_new(Request $request): Response
    {
        $notation = new Notation();
        $form = $this->createForm(NotationType::class, $notation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($notation);
            $entityManager->flush();

            return $this->redirectToRoute('notation_index');
        }

        return $this->render('admin/notation/new.html.twig', [
            'notation' => $notation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/notation/edit/{id}", name="notation_edit", methods={"GET","POST"}, requirements={"id":"\d+"})
     */
    public function notation_edit(Request $request, Notation $notation): Response
    {
        $form = $this->createForm(NotationType::class, $notation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('notation_index');
        }

        return $this->render('admin/notation/edit.html.twig', [
            'notation' => $notation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/notation/delete/{id}", name="notation_delete", methods={"DELETE"}, requirements={"id":"\d+"})
     */
    public function notation_delete(Request $request, Notation $notation): Response
    {
        if ($this->isCsrfTokenValid('delete'.$notation->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($notation);
            $entityManager->flush();
        }

        return $this->redirectToRoute('notation_index');
    }


    //
    // NOTATION THEME
    //
    /**
     * @Route("/themes", name="notation_theme_index", methods={"GET"})
     */
    public function themes(NotationThemeRepository $notationThemeRepository): Response
    {
        return $this->render('admin/notation_theme/index.html.twig', [
            'notation_themes' => $notationThemeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/theme/{id}", name="notation_theme_show", methods={"GET"}, requirements={"id":"\d+"})
     */
    public function theme_show(NotationTheme $notationTheme): Response
    {
        return $this->render('admin/notation_theme/show.html.twig', [
            'notation_theme' => $notationTheme,
        ]);
    }

    /**
     * @Route("/theme/new", name="notation_theme_new", methods={"GET","POST"}, requirements={"id":"\d+"})
     */
    public function theme_new(Request $request): Response
    {
        $notationTheme = new NotationTheme();
        $form = $this->createForm(NotationThemeType::class, $notationTheme);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($notationTheme);
            $entityManager->flush();

            return $this->redirectToRoute('notation_theme_index');
        }

        return $this->render('admin/notation_theme/new.html.twig', [
            'notation_theme' => $notationTheme,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/theme/edit/{id}", name="notation_theme_edit", methods={"GET","POST"}, requirements={"id":"\d+"})
     */
    public function theme_edit(Request $request, NotationTheme $notationTheme): Response
    {
        $form = $this->createForm(NotationThemeType::class, $notationTheme);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('notation_theme_index');
        }

        return $this->render('admin/notation_theme/edit.html.twig', [
            'notation_theme' => $notationTheme,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/theme/delete/{id}", name="notation_theme_delete", methods={"DELETE"}, requirements={"id":"\d+"})
     */
    public function theme_delete(Request $request, NotationTheme $notationTheme): Response
    {
        if ($this->isCsrfTokenValid('delete'.$notationTheme->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($notationTheme);
            $entityManager->flush();
        }

        return $this->redirectToRoute('notation_theme_index');
    }
}
