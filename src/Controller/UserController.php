<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{

    /**
     * @Route("/my_profile", name="my_profile", methods={"GET"})
     */
    public function show(): Response
    {
        $user = $this->getUser();

        return $this->render('user/my_profile.html.twig', [
            'user' => $user,
        ]);
    }

}
