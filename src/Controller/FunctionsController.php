<?php

namespace App\Controller;

use App\Entity\Functions;
use App\Form\FunctionsType;
use App\Repository\FunctionsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/functions")
 */
class FunctionsController extends AbstractController
{

    /**
     * @Route("/rienrienrien/{id}", name="rienrienrien", methods={"GET"})
     */
    public function show(Functions $function): Response
    {
        return $this->render('admin/functions/show.html.twig', [
            'function' => $function,
        ]);
    }

}
