<?php

namespace App\Controller;

use App\Entity\Game;
use App\Entity\Notation;
use App\Entity\NotationGroup;
use App\Entity\GameCategory;
use App\Form\GameType;
use App\Form\NotationGroupType;
use App\Repository\GameRepository;
use App\Repository\NotationRepository;
use App\Repository\NotationThemeRepository;
use App\Repository\GameCategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Tests\Fixtures\Validation\Category;
use Symfony\Bundle\SecurityBundle\Tests\DependencyInjection\Fixtures\UserProvider\DummyProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/games")
 */

class GameController extends AbstractController
{

    /**
     * @var GameRepository
     */
    private $game_repo;
    /**
     * @var GameCategoryRepository
     */
    private $gameCategory_repo;

    /**
     * @var NotationThemeRepository
     */
    private $notation_themes_repo;

    /**
     * @var NotationRepository
     */
    private $notation_repo;

    public function __construct(GameRepository $game_repo, NotationThemeRepository $notation_themes_repo, NotationRepository $notation_repo, GameCategoryRepository $gameCategory_repo)
    {
        $this->game_repo = $game_repo;
        $this->notation_repo = $notation_repo;
        $this->notation_themes_repo = $notation_themes_repo;
        $this->gameCategory_repo = $gameCategory_repo;
    }

    /**
     * @Route("/", name="games")
     * @return Response
     */
    public function index()
    {
        $games = $this->game_repo->findAll();
        $category = $this->gameCategory_repo->findAll();
        return $this->render('game/index.html.twig', [
            'games' => $games,
            'category' => $category
        ]);
    }

    /**
     * @Route("/{categorie}", name="games_filtered")
     * @param $categorie
     * @param GameCategoryRepository $gameCategoryRepository
     * @param GameRepository $gameRepository
     * @return Response
     */
    public function gamesFiltered(category $categorie)
    {
        $list_games =''; $games = '';
        if($categorie) {
        $list_games = $gameCategoryRepository->findAllByCategory((int) $categorie);
        $qb = $gameRepository->createQueryBuilder('g')
            ->join('game_game_category', 'ggc')
            ->where('ggc.game_category_id', ':categorie')
            ->setParameter('categorie', $categorie);
        $games = $qb->getQuery()->getResult();
        }

        dump($games);
        $games = $this->game_repo->findAll();
        $category = $this->gameCategory_repo->findAll();
        return $this->render('game/index.html.twig', [
            'games' => $games,
            'category' => $category,
            'list_games' => $list_games
        ]);

    }

    /**
     * @Route("/display/{id}", name="game_display", methods={"GET","POST"}, requirements={"id":"\d+"})
     */
    public function game_display(Request $request, Game $game): Response
    {
        $notations = array();
        $averages = array();
        $form_already_submitted = false;
        $user = $this->getUser();
        $comment = "";
        $notation_themes = $this->notation_themes_repo->findAll();

        foreach($notation_themes as $theme){
            $all_notations_of_theme = $this->notation_repo->findAllByGameAndByTheme($game, $theme, 'ASC');
            $sum_average = 0;
            $counter = 0;
            foreach($all_notations_of_theme as $item){
                $sum_average += $item->getNotation() * $item->getPonderation();
                $counter += $item->getPonderation();
            }
            if($counter > 0){
                $averages[$theme->getName()] = round($sum_average/$counter,2);
            }else{
                $averages[$theme->getName()] = 0;
            }
        }

        $group_notations = new NotationGroup();
        $form = $this->createForm(NotationGroupType::class, $group_notations);
        $form->handleRequest($request);

        $notations_query = $this->notation_repo->findAllByGameAndByUser($game, $user, 'ASC');
        if($notations_query){
            $form_already_submitted = true;
            foreach($notations_query as $notation)
            {
                $notations[$notation->getNotationTheme()->getName()] = array(
                    'Notation' => $notation->getNotation(),
                    'Ponderation' => $notation->getPonderation()
                );
                $comment = $notation->getComment();
            }
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $results = array();
            $results[0] = $group_notations->getRating1();
            $results[1] = $group_notations->getRating2();
            $results[2] = $group_notations->getRating3();
            $results[3] = $group_notations->getRating4();
            $results[4] = $group_notations->getRating5();
            $i = 0;

            $entityManager = $this->getDoctrine()->getManager();
            foreach($notation_themes as $theme)
            {
                $notation = new Notation();
                $notation->setGame($game);
                $notation->setNotationTheme($theme);
                $notation->setUser($user);
                $notation->setDate(new \DateTime());
                $notation->setNotation($results[$i]);
                if($group_notations->getComment()) $notation->setComment($group_notations->getComment());
                if(in_array("ROLE_EXPERT", $user->getRoles())){
                    $notation->setPonderation(5);
                }else{
                    $notation->setPonderation(1);
                }
                $entityManager->persist($notation);
                $i++;
            }

            if($notations_query){
                foreach($notations_query as $notation)
                {
                    $entityManager->remove($notation);
                }
            }
            $entityManager->flush();
            return $this->render('home/index.html.twig');
        }

        return $this->render('game/display.html.twig', [
            'game' => $game,
            'themes' => $notation_themes,
            'averages' => $averages,
            'form' => $form->createView(),
            'form_already_submitted' => $form_already_submitted,
            'notations' => $notations,
            'comment' => $comment,
        ]);
    }
}
