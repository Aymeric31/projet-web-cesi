<?php

namespace App\Controller;

use App\Entity\GameCategory;
use App\Form\GameCategoryType;
use App\Repository\GameCategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/category")
 */
class GameCategoryController extends AbstractController
{
    /**
     * @Route("/rienrien{id}", name="rienrienrien", methods={"GET"})
     */
    public function show(GameCategory $gameCategory): Response
    {
        return $this->render('admin/category/show.html.twig', [
            'game_category' => $gameCategory,
        ]);
    }
}
