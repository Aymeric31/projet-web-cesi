<?php

namespace App\Controller;

use App\Entity\Notation;
use App\Form\NotationType;
use App\Repository\NotationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/notation")
 */
class NotationController extends AbstractController
{

    /**
     * @Route("/nope/{id}", name="nope", methods={"GET"})
     */
    public function show(Notation $notation): Response
    {
        return $this->render('admin/notation/show.html.twig', [
            'notation' => $notation,
        ]);
    }

}
