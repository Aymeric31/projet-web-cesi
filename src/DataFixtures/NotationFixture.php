<?php

namespace App\DataFixtures;

use App\Entity\Notation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class NotationFixture extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /*
         * USER                 GAMES                                   NOTATIONTHEME
         * 1 Admin              1 Galerapagos                           1 Notation (générale)
         * 2 Expert             2 Time's Up : Family (Version Verte)    2 Accessibilité
         * 3 User               3 Abyss                                 3 Rejouabilité
         * 4 User               4 Mimtoo                                4 Rapport qualité/prix
         * 5 User               5 Dixit                                 5 Ambiance/Fun
         *                      6 Zombicide : Black Plague
         *                      7 Papayoo
         *                      8 Bazar Bizarre 2.0
         *                      9 Pickomino : La Totale !
         */

        $game1 = $this->getReference('game1');
        $game2 = $this->getReference('game2');
        $game3 = $this->getReference('game3');
        $game4 = $this->getReference('game4');
        $game5 = $this->getReference('game5');
        $game6 = $this->getReference('game6');
        $game7 = $this->getReference('game7');
        $game8 = $this->getReference('game8');
        $game9 = $this->getReference('game9');

        $theme1 = $this->getReference('theme1');
        $theme2 = $this->getReference('theme2');
        $theme3 = $this->getReference('theme3');
        $theme4 = $this->getReference('theme4');
        $theme5 = $this->getReference('theme5');

        $user1 = $this->getReference('user1');
        $user2 = $this->getReference('user2');
        $user3 = $this->getReference('user3');
        $user4 = $this->getReference('user4');
        $user5 = $this->getReference('user5');

        $notation1_1 = new Notation();
        $notation1_1->setGame($game1);
        $notation1_1->setNotationTheme($theme1);
        $notation1_1->setUser($user2);
        $notation1_1->setDate(new \DateTime());
        $notation1_1->setNotation(10);
        $notation1_1->setPonderation(5);
        $notation1_1->setComment('Ce jeu mérite cette note');
        $manager->persist($notation1_1);

        $notation1_2 = new Notation();
        $notation1_2->setGame($game1);
        $notation1_2->setNotationTheme($theme2);
        $notation1_2->setUser($user2);
        $notation1_2->setDate(new \DateTime());
        $notation1_2->setNotation(10);
        $notation1_2->setPonderation(5);
        $notation1_2->setComment('Ce jeu mérite cette note');
        $manager->persist($notation1_2);

        $notation1_3 = new Notation();
        $notation1_3->setGame($game1);
        $notation1_3->setNotationTheme($theme3);
        $notation1_3->setUser($user2);
        $notation1_3->setDate(new \DateTime());
        $notation1_3->setNotation(10);
        $notation1_3->setPonderation(5);
        $notation1_3->setComment('Ce jeu mérite cette note');
        $manager->persist($notation1_3);

        $notation1_4 = new Notation();
        $notation1_4->setGame($game1);
        $notation1_4->setNotationTheme($theme4);
        $notation1_4->setUser($user2);
        $notation1_4->setDate(new \DateTime());
        $notation1_4->setNotation(9);
        $notation1_4->setPonderation(5);
        $notation1_4->setComment('Ce jeu mérite cette note');
        $manager->persist($notation1_4);

        $notation1_5 = new Notation();
        $notation1_5->setGame($game1);
        $notation1_5->setNotationTheme($theme5);
        $notation1_5->setUser($user2);
        $notation1_5->setDate(new \DateTime());
        $notation1_5->setNotation(9);
        $notation1_5->setPonderation(5);
        $notation1_5->setComment('Ce jeu mérite cette note');
        $manager->persist($notation1_5);

        // ***********************************************

        $notation2_1 = new Notation();
        $notation2_1->setGame($game1);
        $notation2_1->setNotationTheme($theme1);
        $notation2_1->setUser($user3);
        $notation2_1->setDate(new \DateTime());
        $notation2_1->setNotation(9);
        $notation2_1->setPonderation(1);
        $manager->persist($notation2_1);

        $notation2_2 = new Notation();
        $notation2_2->setGame($game1);
        $notation2_2->setNotationTheme($theme2);
        $notation2_2->setUser($user3);
        $notation2_2->setDate(new \DateTime());
        $notation2_2->setNotation(7);
        $notation2_2->setPonderation(1);
        $manager->persist($notation2_2);

        $notation2_3 = new Notation();
        $notation2_3->setGame($game1);
        $notation2_3->setNotationTheme($theme3);
        $notation2_3->setUser($user3);
        $notation2_3->setDate(new \DateTime());
        $notation2_3->setNotation(9);
        $notation2_3->setPonderation(1);
        $manager->persist($notation2_3);

        $notation2_4 = new Notation();
        $notation2_4->setGame($game1);
        $notation2_4->setNotationTheme($theme4);
        $notation2_4->setUser($user3);
        $notation2_4->setDate(new \DateTime());
        $notation2_4->setNotation(7);
        $notation2_4->setPonderation(1);
        $manager->persist($notation2_4);

        $notation2_5 = new Notation();
        $notation2_5->setGame($game1);
        $notation2_5->setNotationTheme($theme5);
        $notation2_5->setUser($user3);
        $notation2_5->setDate(new \DateTime());
        $notation2_5->setNotation(9);
        $notation2_5->setPonderation(1);
        $manager->persist($notation2_5);

        // ***********************************************

        $notation3_1 = new Notation();
        $notation3_1->setGame($game4);
        $notation3_1->setNotationTheme($theme1);
        $notation3_1->setUser($user4);
        $notation3_1->setDate(new \DateTime());
        $notation3_1->setNotation(6);
        $notation3_1->setPonderation(1);
        $manager->persist($notation3_1);

        $notation3_2 = new Notation();
        $notation3_2->setGame($game4);
        $notation3_2->setNotationTheme($theme2);
        $notation3_2->setUser($user4);
        $notation3_2->setDate(new \DateTime());
        $notation3_2->setNotation(8);
        $notation3_2->setPonderation(1);
        $manager->persist($notation3_2);

        $notation3_3 = new Notation();
        $notation3_3->setGame($game4);
        $notation3_3->setNotationTheme($theme3);
        $notation3_3->setUser($user4);
        $notation3_3->setDate(new \DateTime());
        $notation3_3->setNotation(7);
        $notation3_3->setPonderation(1);
        $manager->persist($notation3_3);

        $notation3_4 = new Notation();
        $notation3_4->setGame($game4);
        $notation3_4->setNotationTheme($theme4);
        $notation3_4->setUser($user4);
        $notation3_4->setDate(new \DateTime());
        $notation3_4->setNotation(8);
        $notation3_4->setPonderation(1);
        $manager->persist($notation3_4);

        $notation3_5 = new Notation();
        $notation3_5->setGame($game4);
        $notation3_5->setNotationTheme($theme5);
        $notation3_5->setUser($user4);
        $notation3_5->setDate(new \DateTime());
        $notation3_5->setNotation(9);
        $notation3_5->setPonderation(1);
        $manager->persist($notation3_5);

        // ***********************************************

        $notation4_1 = new Notation();
        $notation4_1->setGame($game1);
        $notation4_1->setNotationTheme($theme1);
        $notation4_1->setUser($user5);
        $notation4_1->setDate(new \DateTime());
        $notation4_1->setNotation(5);
        $notation4_1->setPonderation(1);
        $manager->persist($notation4_1);

        $notation4_2 = new Notation();
        $notation4_2->setGame($game1);
        $notation4_2->setNotationTheme($theme2);
        $notation4_2->setUser($user5);
        $notation4_2->setDate(new \DateTime());
        $notation4_2->setNotation(4);
        $notation4_2->setPonderation(1);
        $manager->persist($notation4_2);

        $notation4_3 = new Notation();
        $notation4_3->setGame($game1);
        $notation4_3->setNotationTheme($theme3);
        $notation4_3->setUser($user5);
        $notation4_3->setDate(new \DateTime());
        $notation4_3->setNotation(7);
        $notation4_3->setPonderation(1);
        $manager->persist($notation4_3);

        $notation4_4 = new Notation();
        $notation4_4->setGame($game1);
        $notation4_4->setNotationTheme($theme4);
        $notation4_4->setUser($user5);
        $notation4_4->setDate(new \DateTime());
        $notation4_4->setNotation(5);
        $notation4_4->setPonderation(1);
        $manager->persist($notation4_4);

        $notation4_5 = new Notation();
        $notation4_5->setGame($game1);
        $notation4_5->setNotationTheme($theme5);
        $notation4_5->setUser($user5);
        $notation4_5->setDate(new \DateTime());
        $notation4_5->setNotation(9);
        $notation4_5->setPonderation(1);
        $manager->persist($notation4_5);

        $manager->flush();
    }

    public function getOrder()
    {
        return 10;
    }
}
