<?php

namespace App\DataFixtures;

use App\Entity\GameCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixture extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $category1 = new GameCategory();
        $category1->setName("Familial");
        $manager->persist($category1);
        $this->addReference('category1', $category1);

        $category2 = new GameCategory();
        $category2->setName("Tuiles");
        $manager->persist($category2);
        $this->addReference('category2', $category2);

        $category3 = new GameCategory();
        $category3->setName("Jeux de Cartes");
        $manager->persist($category3);
        $this->addReference('category3', $category3);

        $category4 = new GameCategory();
        $category4->setName("Ambiance");
        $manager->persist($category4);
        $this->addReference('category4', $category4);

        $category5 = new GameCategory();
        $category5->setName("Jeu de rôle");
        $manager->persist($category5);
        $this->addReference('category5', $category5);

        $category6 = new GameCategory();
        $category6->setName("Enfant");
        $manager->persist($category6);
        $this->addReference('category6', $category6);

        $category7 = new GameCategory();
        $category7->setName("Jeu de Dés");
        $manager->persist($category7);
        $this->addReference('category7', $category7);

        $category8 = new GameCategory();
        $category8->setName("Plateau");
        $manager->persist($category8);
        $this->addReference('category8', $category8);

        $category9 = new GameCategory();
        $category9->setName("Jeu d'histoire");
        $manager->persist($category9);
        $this->addReference('category9', $category9);

        $category10 = new GameCategory();
        $category10->setName("Imagination");
        $manager->persist($category10);
        $this->addReference('category10', $category10);

        $category11 = new GameCategory();
        $category11->setName("Coopératif");
        $manager->persist($category11);
        $this->addReference('category11', $category11);

        $category12 = new GameCategory();
        $category12->setName("Versus");
        $manager->persist($category12);
        $this->addReference('category12', $category12);

        $category13 = new GameCategory();
        $category13->setName("Équipes");
        $manager->persist($category13);
        $this->addReference('category13', $category13);

        $manager->flush();
    }

    public function getOrder()
    {
        return 4;
    }
}
