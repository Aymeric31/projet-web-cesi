<?php

namespace App\DataFixtures;

use App\Entity\NotationTheme;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class NotationThemeFixture extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $theme = new NotationTheme();
        $theme->setName("Notation");
        $theme->setWeakword("Globalement bof");
        $theme->setStrongword("Plutôt bien");
        $theme->setLimitNotation(10);
        $theme->setIcon('fas fa-grin-hearts');
        $theme->setColor('#e772d6');
        $manager->persist($theme);
        $this->addReference('theme1', $theme);

        $theme2 = new NotationTheme();
        $theme2->setName("Accessibilité");
        $theme2->setWeakword("Plutôt simple");
        $theme2->setStrongword("Douloureusement difficile");
        $theme2->setLimitNotation(10);
        $theme2->setIcon('fas fa-skull');
        $theme2->setColor('#8a451c');
        $manager->persist($theme2);
        $this->addReference('theme2', $theme2);

        $theme3 = new NotationTheme();
        $theme3->setName("Rejouabilité");
        $theme3->setWeakword("Se joue une seule fois");
        $theme3->setStrongword("Parties infinies");
        $theme3->setLimitNotation(10);
        $theme3->setIcon('fas fa-undo');
        $theme3->setColor('#51e753');
        $manager->persist($theme3);
        $this->addReference('theme3', $theme3);

        $theme4 = new NotationTheme();
        $theme4->setName("Rapport qualité/prix");
        $theme4->setWeakword("Bon marché");
        $theme4->setStrongword("Investissement important");
        $theme4->setLimitNotation(10);
        $theme4->setIcon('fas fa-coins');
        $theme4->setColor('#d6af00');
        $manager->persist($theme4);
        $this->addReference('theme4', $theme4);

        $theme5 = new NotationTheme();
        $theme5->setName("Ambiance/Fun");
        $theme5->setWeakword("Chiant à mourir");
        $theme5->setStrongword("Idéal pour une soirée");
        $theme5->setLimitNotation(10);
        $theme5->setIcon('far fa-grin-squint-tears');
        $theme5->setColor('#e72118');
        $manager->persist($theme5);
        $this->addReference('theme5', $theme5);

        $manager->flush();
    }

    public function getOrder()
    {
        return 9;
    }
}
