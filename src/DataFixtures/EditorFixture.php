<?php

namespace App\DataFixtures;

use App\Entity\Editor;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class EditorFixture extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $editor = new Editor();
        $editor->setName("Gigamic");
        $editor->setWebsite("https://www.gigamic.com/");
        $editor->setAddress("Parc naturel régional des Caps et Marais d Opale, 22 Rue Jean Marie Bourguignon, 62930 Wimereux");
        $manager->persist($editor);
        $this->addReference('editor1', $editor);

        $editor2 = new Editor();
        $editor2->setName("Asmodee");
        $editor2->setWebsite("https://corporate.asmodee.com/");
        $editor2->setAddress("18 Rue Jacqueline Auriol, 78280 Guyancourt");
        $manager->persist($editor2);
        $this->addReference('editor2', $editor2);

        $editor3 = new Editor();
        $editor3->setName("Bombyx");
        $editor3->setWebsite("https://studiobombyx.com/");
        $editor3->setAddress("8 Rue du Palais, 29000 Quimper");
        $manager->persist($editor3);
        $this->addReference('editor3', $editor3);

        $editor4 = new Editor();
        $editor4->setName("Cocktail Games");
        $editor4->setWebsite("http://www.cocktailgames.com/");
        $editor4->setAddress("2 Rue du Hazard, 78000 Versailles");
        $manager->persist($editor4);
        $this->addReference('editor4', $editor4);

        $editor5 = new Editor();
        $editor5->setName("Libellud");
        $editor5->setWebsite("http://www.libellud.com/");
        $editor5->setAddress("23 Rue Alsace Lorraine, 86000 Poitiers");
        $manager->persist($editor5);
        $this->addReference('editor5', $editor5);

        $editor6 = new Editor();
        $editor6->setName("Edge Entertainment");
        $editor6->setWebsite("http://www.edgeent.fr/");
        $editor6->setAddress("1 Rond-Point de Flotis, Bâtiment 3, 1er Étage, 31240 Saint-Jean");
        $manager->persist($editor6);
        $this->addReference('editor6', $editor6);

        $editor7 = new Editor();
        $editor7->setName("Iello");
        $editor7->setWebsite("https://www.iello.com/");
        $editor7->setAddress("9 Avenue des Érables, 54180 Heillecourt");
        $manager->persist($editor7);
        $this->addReference('editor7', $editor7);

        $manager->flush();
    }

    public function getOrder()
    {
        return 5;
    }
}
