<?php

namespace App\DataFixtures;

use App\Entity\Functions;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class FunctionsFixture extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $myfunction1 = new Functions();
        $myfunction1->setName("Boutique (jeux)");
        $manager->persist($myfunction1);
        $this->addReference('function1', $myfunction1);

        $myfunction2 = new Functions();
        $myfunction2->setName("Boutique (goodies)");
        $manager->persist($myfunction2);
        $this->addReference('function2', $myfunction2);

        $myfunction3 = new Functions();
        $myfunction3->setName("Bar");
        $manager->persist($myfunction3);
        $this->addReference('function3', $myfunction3);

        $myfunction4 = new Functions();
        $myfunction4->setName("Bar (sans alcool)");
        $manager->persist($myfunction4);
        $this->addReference('function4', $myfunction4);

        $myfunction5 = new Functions();
        $myfunction5->setName("Restauration");
        $manager->persist($myfunction5);
        $this->addReference('function5', $myfunction5);

        $myfunction6 = new Functions();
        $myfunction6->setName("Restauration (tapas)");
        $manager->persist($myfunction6);
        $this->addReference('function6', $myfunction6);

        $myfunction7 = new Functions();
        $myfunction7->setName("Jeux à l'essai (libre et gratuit)");
        $manager->persist($myfunction7);
        $this->addReference('function7', $myfunction7);

        $myfunction8 = new Functions();
        $myfunction8->setName("Jeux en accès libre (payant)");
        $manager->persist($myfunction8);
        $this->addReference('function8', $myfunction8);

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }

}
