<?php

namespace App\DataFixtures;

use App\Entity\Barshop;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class BarshopFixture extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $function1 = $this->getReference('function1'); // 1 "Boutique (jeux)"
        $function2 = $this->getReference('function2'); // 2 "Boutique (goodies)"
        $function3 = $this->getReference('function3'); // 3 "Bar"
        $function4 = $this->getReference('function4'); // 4 "Bar (sans alcool)"
        $function5 = $this->getReference('function5'); // 5 "Restauration"
        $function6 = $this->getReference('function6'); // 6 "Restauration (tapas)"
        $function7 = $this->getReference('function7'); // 7 "Jeux à l'essai (libre et gratuit)"
        $function8 = $this->getReference('function8'); // 8 "Jeux en accès libre (payant)"

        $barshop1 = new Barshop();
        $barshop1->setName("Blastodice");
        $barshop1->setImage("images/ludopawn/barshops/blastodice.jpg");
        $barshop1->setWordOfTheBoss("Viens on est bien...");
        $barshop1->setDescription("Bar à jeux, des tables jusqu'à 12 personnes... Nous proposons tout un tas de jeux de société et autres accessoires en lien avec le monde ludique des petits et des grands. Mange tes morts. Vous trouverez tout ce dont vous aurez besoin pour passer des soirées très animées avec vos amis ou votre famille, jeux de plateau, jeux de dés, jeux traditionnels...");
        $barshop1->setAddressNum("52");
        $barshop1->setAddressStreetOrLocality("Avenue Honoré Serres");
        $barshop1->setAddressComplement("");
        $barshop1->setAddressZipcode("31000");
        $barshop1->setAddressCity("Toulouse");
        $barshop1->setPhone("0986725181");
        $barshop1->setEmail("fauxmail@test.fr");
        $barshop1->setWebsite("https://www.blastodice.com/");
        $barshop1->setType("bar");
        $barshop1->addFunction($function3);
        $barshop1->addFunction($function6);
        $barshop1->addFunction($function8);
        $manager->persist($barshop1);
        $this->addReference('barshop1', $barshop1);

        $barshop2 = new Barshop();
        $barshop2->setName("Baraka Jeux");
        $barshop2->setImage("images/ludopawn/barshops/baraka.jpg");
        $barshop2->setWordOfTheBoss("Cosy dechez cosy");
        $barshop2->setDescription("Bar à jeux, des rondes de loup garou le mercredi soir en hiver jusqu'à 25 personnes, vous pouvez commander à manger à l'extérieur. Notre boutique de jeux et accessoires propose tout un tas de jeux de société et autres accessoires en lien avec le monde ludique des petits et des grands. Vous trouverez tout ce dont vous aurez besoin pour passer des soirées très animées avec vos amis ou votre famille, jeux de plateau, jeux de dés, jeux traditionnels, jeux tous âges...");
        $barshop2->setAddressNum("1");
        $barshop2->setAddressStreetOrLocality("Boulevard de la Gare");
        $barshop2->setAddressComplement("");
        $barshop2->setAddressZipcode("31500");
        $barshop2->setAddressCity("Toulouse");
        $barshop2->setPhone("0951575685");
        $barshop2->setEmail("");
        $barshop2->setWebsite("https://baraka-jeux.com/");
        $barshop2->setType("bar");
        $barshop2->addFunction($function3);
        $barshop2->addFunction($function8);
        $manager->persist($barshop2);
        $this->addReference('barshop2', $barshop2);

        $barshop3 = new Barshop();
        $barshop3->setName("Les tricheurs");
        $barshop3->setImage("images/ludopawn/barshops/tricheurs.jpg");
        $barshop3->setWordOfTheBoss("Tricher c'est mal, pour gagner ça peut passer...");
        $barshop3->setDescription("Situé entre la place du mélange cannabis-urine et la place des beuveries... Notre magasin de jeux et accessoires propose tout un tas de jeux de société et autres objets contondants en lien avec le monde ludique des petits et des glands. Vous trouverez tout ce dont vous aurez besoin pour passer des soirées très animées avec vos amis ou votre famille (même si là c'est un peu bizarre), jeux de plateau, jeux de dés, rami, twister, jeux tous âges...");
        $barshop3->setAddressNum("34");
        $barshop3->setAddressStreetOrLocality("Rue des Blanchers");
        $barshop3->setAddressComplement("");
        $barshop3->setAddressZipcode("31000");
        $barshop3->setAddressCity("Toulouse");
        $barshop3->setPhone("05 61 23 04 32");
        $barshop3->setEmail("");
        $barshop3->setWebsite("https://www.lestricheurs.com/");
        $barshop3->setType("bar");
        $barshop3->addFunction($function3);
        $barshop3->addFunction($function6);
        $barshop3->addFunction($function8);
        $manager->persist($barshop3);
        $this->addReference('barshop3', $barshop3);

        $barshop4 = new Barshop();
        $barshop4->setName("Le Passe Temps");
        $barshop4->setImage("images/ludopawn/barshops/passetemps.jpg");
        $barshop4->setWordOfTheBoss("Notre boutique est plutôt correcte");
        $barshop4->setDescription("Dans la rue des lois, notre boutique de jeux et accessoires propose tout un tas de jeux de société et autres accessoires en lien avec le monde ludique des petits et des grands. Vous trouverez tout ce dont vous aurez besoin pour passer des soirées très animées avec vos amis ou votre famille, jeux de plateau, jeux de dés, jeux traditionnels, jeux tous âges...");
        $barshop4->setAddressNum("30");
        $barshop4->setAddressStreetOrLocality("Rue des Lois");
        $barshop4->setAddressComplement("");
        $barshop4->setAddressZipcode("31000");
        $barshop4->setAddressCity("Toulouse");
        $barshop4->setPhone("05 61 22 60 20");
        $barshop4->setEmail("bullshit@test.fr");
        $barshop4->setWebsite("https://www.le-passe-temps.com/");
        $barshop4->setType("boutique");
        $barshop4->addFunction($function1);
        $barshop4->addFunction($function2);
        $barshop4->addFunction($function7);
        $manager->persist($barshop4);
        $this->addReference('barshop4', $barshop4);

        $barshop5 = new Barshop();
        $barshop5->setName("C'est le jeu");
        $barshop5->setImage("images/ludopawn/barshops/cestlejeu.jpg");
        $barshop5->setWordOfTheBoss("Notre boutique est jolie");
        $barshop5->setDescription("Dans la rue Gambetta, boutique de jeux et accessoires en tout genre... Notre boutique de jeux et accessoires propose tout un tas de jeux de société et autres accessoires en lien avec le monde ludique des petits et des grands. Vous trouverez tout ce dont vous aurez besoin pour passer des soirées très animées avec vos amis ou votre famille, jeux de plateau, jeux de dés, jeux de rôles... Nous organisons également des parties de Loup Garou de Thiercelieux personnalisée et gratuites tous les derniers mercredi du mois.");
        $barshop5->setAddressNum("52");
        $barshop5->setAddressStreetOrLocality("Rue Léon Gambetta");
        $barshop5->setAddressComplement("");
        $barshop5->setAddressZipcode("31000");
        $barshop5->setAddressCity("Toulouse");
        $barshop5->setPhone("05 31 15 18 33");
        $barshop5->setEmail("");
        $barshop5->setWebsite("http://www.cestlejeu.com/");
        $barshop5->setType("boutique");
        $barshop5->addFunction($function1);
        $barshop5->addFunction($function2);
        $barshop5->addFunction($function7);
        $manager->persist($barshop5);
        $this->addReference('barshop5', $barshop5);

        $barshop6 = new Barshop();
        $barshop6->setName("Oxybul Eveil et Jeux");
        $barshop6->setImage("images/ludopawn/barshops/oxybul-capitole.jpg");
        $barshop6->setWordOfTheBoss("Gnégnégné vegnez jouer");
        $barshop6->setDescription("Près du Capitole, Vous trouverez tout ce dont vous aurez besoin pour passer des soirées très animées avec vos amis ou votre famille, jeux de plateau, jeux de dés, jeux traditionnels, jeux tous âges... Notre boutique de jeux et accessoires propose tout un tas de jeux de société et autres accessoires en lien avec le monde ludique des petits et des grands.");
        $barshop6->setAddressNum("1");
        $barshop6->setAddressStreetOrLocality("Place du Capitole");
        $barshop6->setAddressComplement("");
        $barshop6->setAddressZipcode("31000");
        $barshop6->setAddressCity("Toulouse");
        $barshop6->setPhone("05 61 23 11 82");
        $barshop6->setEmail("tatata@test.fr");
        $barshop6->setWebsite("https://www.oxybul.com/magasins-jouets/magasin-toulouse-capitole/35");
        $barshop6->setType("boutique");
        $barshop6->addFunction($function1);
        $barshop6->addFunction($function7);
        $manager->persist($barshop6);
        $this->addReference('barshop6', $barshop6);

        $barshop7 = new Barshop();
        $barshop7->setName("Jeux Barjo - Boutique");
        $barshop7->setImage("images/ludopawn/barshops/no-image.jpg");
        $barshop7->setWordOfTheBoss("On ne vend pas que du vin, à Bordeaux...");
        $barshop7->setDescription("Près du Centre, Vous trouverez tout ce dont vous aurez besoin pour passer des soirées très animées avec vos amis ou votre famille, jeux de plateau, jeux de dés, jeux traditionnels, jeux tous âges... Notre boutique de jeux et accessoires propose tout un tas de jeux de société et autres accessoires en lien avec le monde ludique des petits et des grands.");
        $barshop7->setAddressNum("26");
        $barshop7->setAddressStreetOrLocality("Rue des Ayres");
        $barshop7->setAddressComplement("");
        $barshop7->setAddressZipcode("33000");
        $barshop7->setAddressCity("Bordeaux");
        $barshop7->setPhone("09 86 35 99 93");
        $barshop7->setEmail("boutique-test@test.fr");
        $barshop7->setWebsite("https://www.facebook.com/jeuxbarjobordeaux");
        $barshop7->setType("boutique");
        $barshop7->addFunction($function1);
        $barshop7->addFunction($function2);
        $barshop7->addFunction($function7);
        $manager->persist($barshop7);
        $this->addReference('barshop7', $barshop7);

        $barshop8 = new Barshop();
        $barshop8->setName("Jeux Barjo - Bar");
        $barshop8->setImage("images/ludopawn/barshops/no-image.jpg");
        $barshop8->setWordOfTheBoss("On ne vend pas que du vin, à Bordeaux...");
        $barshop8->setDescription("Près du Centre, Vous trouverez tout ce dont vous aurez besoin pour passer des soirées très animées avec vos amis ou votre famille, jeux de plateau, jeux de dés, jeux traditionnels, jeux tous âges... Notre boutique de jeux et accessoires propose tout un tas de jeux de société et autres accessoires en lien avec le monde ludique des petits et des grands.");
        $barshop8->setAddressNum("12");
        $barshop8->setAddressStreetOrLocality("Rue Saint-James");
        $barshop8->setAddressComplement("");
        $barshop8->setAddressZipcode("33000");
        $barshop8->setAddressCity("Bordeaux");
        $barshop8->setPhone("09 86 35 99 93");
        $barshop8->setEmail("boutique-test@test.fr");
        $barshop8->setWebsite("https://www.facebook.com/jeuxbarjobordeaux");
        $barshop8->setType("bar");
        $barshop8->addFunction($function3);
        $barshop8->addFunction($function6);
        $barshop8->addFunction($function8);
        $manager->persist($barshop8);
        $this->addReference('barshop8', $barshop8);

        $barshop9 = new Barshop();
        $barshop9->setName("Jeux Descartes");
        $barshop9->setImage("images/ludopawn/barshops/no-image.jpg");
        $barshop9->setWordOfTheBoss("On aime les jeux de mots ici...");
        $barshop9->setDescription("Vous trouverez tout ce dont vous aurez besoin pour passer des soirées très animées avec vos amis ou votre famille, jeux de plateau, jeux de dés, jeux traditionnels, jeux tous âges... Notre boutique de jeux et accessoires propose tout un tas de jeux de société et autres accessoires en lien avec le monde ludique des petits et des grands.");
        $barshop9->setAddressNum("69 Bis");
        $barshop9->setAddressStreetOrLocality("Rue des Trois-Conils");
        $barshop9->setAddressComplement("");
        $barshop9->setAddressZipcode("33000");
        $barshop9->setAddressCity("Bordeaux");
        $barshop9->setPhone("05 57 35 71 21");
        $barshop9->setEmail("boutik-test@web.com");
        $barshop9->setWebsite("https://www.facebook.com/Jeux.Descartes.Bordeaux/");
        $barshop9->setType("boutique");
        $barshop9->addFunction($function1);
        $barshop9->addFunction($function2);
        $barshop9->addFunction($function7);
        $manager->persist($barshop9);
        $this->addReference('barshop9', $barshop9);

        $barshop10 = new Barshop();
        $barshop10->setName("R 2 Jeux");
        $barshop10->setImage("images/ludopawn/barshops/no-image.jpg");
        $barshop10->setWordOfTheBoss("Magasin de jouets");
        $barshop10->setDescription("Vous trouverez tout ce dont vous aurez besoin pour passer des soirées très animées avec vos amis ou votre famille, jeux de plateau, jeux de dés, jeux traditionnels, jeux tous âges... Notre boutique de jeux et accessoires propose tout un tas de jeux de société et autres accessoires en lien avec le monde ludique des petits et des grands.");
        $barshop10->setAddressNum("41");
        $barshop10->setAddressStreetOrLocality("Rue Saint-Joseph");
        $barshop10->setAddressComplement("");
        $barshop10->setAddressZipcode("33000");
        $barshop10->setAddressCity("Bordeaux");
        $barshop10->setPhone("06 63 73 13 93");
        $barshop10->setEmail("magasin@web.com");
        $barshop10->setWebsite("https://www.r2jeux.org/");
        $barshop10->setType("boutique");
        $barshop10->addFunction($function1);
        $barshop10->addFunction($function2);
        $barshop10->addFunction($function7);
        $manager->persist($barshop10);
        $this->addReference('barshop10', $barshop10);

        $barshop11 = new Barshop();
        $barshop11->setName("Pirouettes");
        $barshop11->setImage("images/ludopawn/barshops/no-image.jpg");
        $barshop11->setWordOfTheBoss("Magasin de jouets aussi");
        $barshop11->setDescription("Vous trouverez tout ce dont vous aurez besoin pour passer des soirées très animées avec vos amis ou votre famille, jeux de plateau, jeux de dés, jeux traditionnels, jeux tous âges... Notre boutique de jeux et accessoires propose tout un tas de jeux de société et autres accessoires en lien avec le monde ludique des petits et des grands.");
        $barshop11->setAddressNum("130");
        $barshop11->setAddressStreetOrLocality("Cours de Verdun");
        $barshop11->setAddressComplement("");
        $barshop11->setAddressZipcode("33000");
        $barshop11->setAddressCity("Bordeaux");
        $barshop11->setPhone("09 81 05 70 82");
        $barshop11->setEmail("magasin@nul.com");
        $barshop11->setWebsite("http://33pirouettes.fr/");
        $barshop11->setType("boutique");
        $barshop11->addFunction($function1);
        $barshop11->addFunction($function2);
        $barshop11->addFunction($function7);
        $manager->persist($barshop11);
        $this->addReference('barshop11', $barshop11);

        $barshop12 = new Barshop();
        $barshop12->setName("PopGame");
        $barshop12->setImage("images/ludopawn/barshops/no-image.jpg");
        $barshop12->setWordOfTheBoss("Blablabla aucune idée");
        $barshop12->setDescription("Vous trouverez tout ce dont vous aurez besoin pour passer des soirées très animées avec vos amis ou votre famille, jeux de plateau, jeux de dés, jeux traditionnels, jeux tous âges... Notre boutique de jeux et accessoires propose tout un tas de jeux de société et autres accessoires en lien avec le monde ludique des petits et des grands.");
        $barshop12->setAddressNum("20");
        $barshop12->setAddressStreetOrLocality("Place Saint-Martial");
        $barshop12->setAddressComplement("");
        $barshop12->setAddressZipcode("33300");
        $barshop12->setAddressCity("Bordeaux");
        $barshop12->setPhone("06 73 50 35 34");
        $barshop12->setEmail("popgame@test.com");
        $barshop12->setWebsite("https://www.popgame.fr/");
        $barshop12->setType("boutique");
        $barshop12->addFunction($function1);
        $barshop12->addFunction($function2);
        $barshop12->addFunction($function7);
        $manager->persist($barshop12);
        $this->addReference('barshop12', $barshop12);

        $barshop13 = new Barshop();
        $barshop13->setName("Univers Parallèle");
        $barshop13->setImage("images/ludopawn/barshops/no-image.jpg");
        $barshop13->setWordOfTheBoss("Un autre univers pas perpendiculaire");
        $barshop13->setDescription("Vous trouverez tout ce dont vous aurez besoin pour passer des soirées très animées avec vos amis ou votre famille, jeux de plateau, jeux de dés, jeux traditionnels, jeux tous âges... Notre boutique de jeux et accessoires propose tout un tas de jeux de société et autres accessoires en lien avec le monde ludique des petits et des grands.");
        $barshop13->setAddressNum("36");
        $barshop13->setAddressStreetOrLocality("Rue Sainte-Ursule");
        $barshop13->setAddressComplement("");
        $barshop13->setAddressZipcode("31000");
        $barshop13->setAddressCity("Toulouse");
        $barshop13->setPhone("05 62 30 84 74");
        $barshop13->setEmail("lalala@test.com");
        $barshop13->setWebsite("https://www.univers-parallele.fr/");
        $barshop13->setType("boutique");
        $barshop13->addFunction($function1);
        $barshop13->addFunction($function2);
        $barshop13->addFunction($function7);
        $manager->persist($barshop13);
        $this->addReference('barshop13', $barshop13);

        $barshop14 = new Barshop();
        $barshop14->setName("La Grande Récré");
        $barshop14->setImage("images/ludopawn/barshops/no-image.jpg");
        $barshop14->setWordOfTheBoss("La vache dans les prés");
        $barshop14->setDescription("Vous trouverez tout ce dont vous aurez besoin pour passer des soirées très animées avec vos amis ou votre famille, jeux de plateau, jeux de dés, jeux traditionnels, jeux tous âges... Notre boutique de jeux et accessoires propose tout un tas de jeux de société et autres accessoires en lien avec le monde ludique des petits et des grands.");
        $barshop14->setAddressNum("55");
        $barshop14->setAddressStreetOrLocality("Rue Saint-Rome");
        $barshop14->setAddressComplement("");
        $barshop14->setAddressZipcode("31000");
        $barshop14->setAddressCity("Toulouse");
        $barshop14->setPhone("05 61 29 07 15");
        $barshop14->setEmail("lgrécré@test.com");
        $barshop14->setWebsite("https://www.lagranderecre.fr/");
        $barshop14->setType("boutique");
        $barshop14->addFunction($function1);
        $barshop14->addFunction($function2);
        $barshop14->addFunction($function7);
        $manager->persist($barshop14);
        $this->addReference('barshop14', $barshop14);

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
