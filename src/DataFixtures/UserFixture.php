<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends Fixture implements OrderedFixtureInterface
{
    private $passwordEncoder;
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setFirstname("Admin");
        $user->setLastname("ADMIN");
        $user->setEmail("admin@mail.com");
        $user->setUsername("admin");
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'ludopawn'
        ));
        $user->setAvatar("images/avatar/no_avatar.png");
        $user->setRoles(["ROLE_ADMIN"]);
        $user->setDateSubscribe(new \DateTime());
        $manager->persist($user);
        $this->addReference('user1', $user);


        $user2 = new User();
        $user2->setFirstname("Jean");
        $user2->setLastname("PEUPLU");
        $user2->setEmail("expert@mail.com");
        $user2->setUsername("expert");
        $user2->setPassword($this->passwordEncoder->encodePassword(
            $user2,
            'ludopawn'
        ));        $user2->setAvatar("images/avatar/no_avatar.png");
        $user2->setRoles(["ROLE_EXPERT"]);
        $user2->setDateSubscribe(new \DateTime());
        $manager->persist($user2);
        $this->addReference('user2', $user2);


        $user3 = new User();
        $user3->setFirstname("Sylvie");
        $user3->setLastname("CULTURE");
        $user3->setEmail("user@mail.com");
        $user3->setUsername("user");
        $user3->setPassword($this->passwordEncoder->encodePassword(
            $user3,
            'ludopawn'
        ));        $user3->setAvatar("images/avatar/no_avatar.png");
        $user3->setRoles(["ROLE_USER"]);
        $user3->setDateSubscribe(new \DateTime());
        $manager->persist($user3);
        $this->addReference('user3', $user3);


        $user4 = new User();
        $user4->setFirstname("Laurie");
        $user4->setLastname("PILANTE");
        $user4->setEmail("user2@mail.com");
        $user4->setUsername("user2");
        $user4->setPassword($this->passwordEncoder->encodePassword(
            $user4,
            'ludopawn'
        ));        $user4->setAvatar("images/avatar/no_avatar.png");
        $user4->setRoles(["ROLE_USER"]);
        $user4->setDateSubscribe(new \DateTime());
        $manager->persist($user4);
        $this->addReference('user4', $user4);


        $user5 = new User();
        $user5->setFirstname("Alphonse");
        $user5->setLastname("DANLTA");
        $user5->setEmail("user3@mail.com");
        $user5->setUsername("user3");
        $user5->setPassword($this->passwordEncoder->encodePassword(
            $user5,
            'ludopawn'
        ));        $user5->setAvatar("images/avatar/no_avatar.png");
        $user5->setRoles(["ROLE_USER"]);
        $user5->setDateSubscribe(new \DateTime());
        $manager->persist($user5);
        $this->addReference('user5', $user5);

        $manager->flush();
    }

    public function getOrder()
    {
        return 8;
    }
}
