<?php

namespace App\DataFixtures;

use App\Entity\Game;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class GameFixture extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $category1 = $this->getReference('category1'); // 1 "Familial"
        $category2 = $this->getReference('category2'); // 2 "Jeu de Tuiles"
        $category3 = $this->getReference('category3'); // 3 "Jeux de Cartes"
        $category4 = $this->getReference('category4'); // 4 "Ambiance"
        $category5 = $this->getReference('category5'); // 5 "Jeu de rôle"
        $category6 = $this->getReference('category6'); // 6 "Jeux pour enfants Enfant"
        $category7 = $this->getReference('category7'); // 7 "Jeu de Dés"
        $category8 = $this->getReference('category8'); // 8 "Jeu de plateau"
        $category9 = $this->getReference('category9'); // 9 "Jeu d'aventure"
        $category10 = $this->getReference('category10'); // 10 "Jeu d'imagination"
        $category11 = $this->getReference('category11'); // 11 "Coopératif"
        $category12 = $this->getReference('category12'); // 12 "Versus"
        $category13 = $this->getReference('category13'); // 13 "Équipes"

        $editor1 = $this->getReference('editor1');
        $editor2 = $this->getReference('editor2');
        $editor3 = $this->getReference('editor3');
        $editor4 = $this->getReference('editor4');
        $editor5 = $this->getReference('editor5');
        $editor6 = $this->getReference('editor6');
        $editor7 = $this->getReference('editor7');

        $game = new Game();
        $game->setName("Galerapagos");
        $game->setSynopsys("Dans ce jeu semi-coopératif, les joueurs forment un groupe de rescapés sur une île déserte après le naufrage d'un bateau. Afin de s'en sortir, les survivants n'ont d'autre choix que de s'unir dans la construction d'un radeau.");
        $game->setImage("images/ludopawn/games/galerapagos.jpg");
        $game->setEditor($editor1); // Gigamic
        $game->setAveragePrice(19.90);
        $game->setAgeLimit("10-99");
        $game->setMinutesPerGame("30/45");
        $game->setNumberPlayers("3/12");
        $game->addCategory($category5);
        $game->addCategory($category8);
        $game->addCategory($category11);
        $game->addCategory($category12);
        $manager->persist($game);
        $this->addReference('game1', $game);

        $game2 = new Game();
        $game2->setName("Time's Up : Family (Version Verte)");
        $game2->setSynopsys("La version familiale du célèbre jeu du Time's Up pour que tout le monde puisse jouer ensemble.");
        $game2->setImage("images/ludopawn/games/timesupfamily.jpg");
        $game2->setEditor($editor2); // Asmodee
        $game2->setAveragePrice(20.90);
        $game2->setAgeLimit("4-99");
        $game2->setMinutesPerGame("30/45");
        $game2->setNumberPlayers("3/20");
        $game2->addCategory($category3);
        $game2->addCategory($category4);
        $game2->addCategory($category13);
        $manager->persist($game2);
        $this->addReference('game2', $game2);

        $game3 = new Game();
        $game3->setName("Abyss");
        $game3->setSynopsys("Le pouvoir d'Abyss est de nouveau vacant. Votre heure est venue de mettre la main sur le trône et sur ses privilèges.");
        $game3->setImage("images/ludopawn/games/abyss.jpg");
        $game3->setEditor($editor3); // Bombyx
        $game3->setAveragePrice(35.95);
        $game3->setAgeLimit("14-99");
        $game3->setMinutesPerGame("30/60");
        $game3->setNumberPlayers("2/4");
        $game3->addCategory($category5);
        $game3->addCategory($category8);
        $manager->persist($game3);
        $this->addReference('game3', $game3);

        $game4 = new Game();
        $game4->setName("Mimtoo");
        $game4->setSynopsys("Saurez-vous tout mimer ?﻿");
        $game4->setImage("images/ludopawn/games/mimtoo.jpg");
        $game4->setEditor($editor4); // Cocktail Games
        $game4->setAveragePrice(13.50);
        $game4->setAgeLimit("10-99");
        $game4->setMinutesPerGame("30/60");
        $game4->setNumberPlayers("4/10");
        $game4->addCategory($category3);
        $game4->addCategory($category6);
        $manager->persist($game4);
        $this->addReference('game4', $game4);

        $game5 = new Game();
        $game5->setName("Dixit");
        $game5->setSynopsys("Entrez dans le monde poétique de Dixit. Faites marcher votre imagination pour deviner ou faire deviner les magnifiques cartes illustrées par Marie Cardouat.﻿");
        $game5->setImage("images/ludopawn/games/dixit.jpg");
        $game5->setEditor($editor5); // Libellud
        $game5->setAveragePrice(26.90);
        $game5->setAgeLimit("8-99");
        $game5->setMinutesPerGame("30/60");
        $game5->setNumberPlayers("3/6");
        $game5->addCategory($category3);
        $game5->addCategory($category10);
        $manager->persist($game5);
        $this->addReference('game5', $game5);

        $game6 = new Game();
        $game6->setName("Zombicide : Black Plague");
        $game6->setSynopsys("﻿Bienvenue dans un âge de ténèbres ! Dans leur quête insensée de savoir interdit et de puissance ultime, les nécromanciens ont déclenché l’apocalypse !");
        $game6->setImage("images/ludopawn/games/zombicidebp.jpg");
        $game6->setEditor($editor6); // Edge Entertainment
        $game6->setAveragePrice(89.95);
        $game6->setAgeLimit("12-99");
        $game6->setMinutesPerGame("60/120");
        $game6->setNumberPlayers("1/6");
        $game6->addCategory($category5);
        $game6->addCategory($category8);
        $game6->addCategory($category9);
        $manager->persist($game6);
        $this->addReference('game6', $game6);

        $game7 = new Game();
        $game7->setName("Papayoo");
        $game7->setSynopsys("﻿Il n’y a ici ni valets, ni reines, ni rois, mais une cinquième couleur, dite Payoo, plus un drôle de dé. Comment marquer… le moins de points possible? En évitant de récolter ces fichus Payoos et surtout le Papayoo, ce satané 7 dont la couleur change (maudit dé!) à chaque manche… Vous n’êtes pas satisfait de la donne ? Pas grave, vous donnez des cartes à votre gauche avant de commencer; mais faites les bons choix, car elles seront remplacées par le «cadeau» venant de droite… Ensuite vous jouez la donne mais sans atouts et sans scrupules, pas sûr que le meilleur gagne !!!");
        $game7->setImage("images/ludopawn/games/papayoo.jpg");
        $game7->setEditor($editor1); // Gigamic
        $game7->setAveragePrice(13.90);
        $game7->setAgeLimit("7-99");
        $game7->setMinutesPerGame("30/60");
        $game7->setNumberPlayers("3/8");
        $game7->addCategory($category3);
        $manager->persist($game7);
        $this->addReference('game7', $game7);

        $game8 = new Game();
        $game8->setName("Bazar Bizarre 2.0");
        $game8->setSynopsys("﻿Une fantômesse arrive au château avec de nouveaux objets et elle est bien décidée à mettre encore plus le bazar !");
        $game8->setImage("images/ludopawn/games/bazarbizarre2.jpg");
        $game8->setEditor($editor1); // Gigamic
        $game8->setAveragePrice(15.90);
        $game8->setAgeLimit("6-99");
        $game8->setMinutesPerGame("30/60");
        $game8->setNumberPlayers("2/8");
        $game8->addCategory($category1);
        $game8->addCategory($category6);
        $manager->persist($game8);
        $this->addReference('game8', $game8);

        $game9 = new Game();
        $game9->setName("Pickomino : La Totale !");
        $game9->setSynopsys("﻿C'est reparti pour le festin de vers de terres dans une boite comprenant le jeu de base et l'extension du célèbre jeu de dés et de prise de risque !");
        $game9->setImage("images/ludopawn/games/pickominototale.jpg");
        $game9->setEditor($editor1); // Gigamic
        $game9->setAveragePrice(24.90);
        $game9->setAgeLimit("8-99");
        $game9->setMinutesPerGame("20/30");
        $game9->setNumberPlayers("2/7");
        $game9->addCategory($category1);
        $game9->addCategory($category7);
        $manager->persist($game9);
        $this->addReference('game9', $game9);

        $manager->flush();
    }

    public function getOrder()
    {
        return 6;
    }
}
